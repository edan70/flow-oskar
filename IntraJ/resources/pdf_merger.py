import sys
from PyPDF2 import PdfFileMerger
import os
filename = sys.argv[1]

#cmd = 'cp ' + filename + ' ' + filename + '.java'
# os.system(cmd)
cmd = 'vim ' + filename+'.java' + \
    ' -c ":hardcopy > ' + filename + '.ps ''" -c ": q"'
os.system(cmd)
cmd = 'ps2pdf ' + filename + '.ps ' + filename+'_tmp.pdf'
os.system(cmd)

merger = PdfFileMerger()
merger.append(filename+'_tmp.pdf')
merger.append(filename+'_FNext_CFG.pdf')
merger.write(filename+'_CFG.pdf')

merger.close()

cmd = 'rm ' + filename + '_tmp.pdf ' + filename + '.ps '+filename + '_FNext_CFG.dot ' + \
    filename + '_FNext_CFG.pdf '


os.system(cmd)
