package test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Scanner;

public class UtilTest {
  @SuppressWarnings("javadoc")
  public static Iterable<Object[]>
  getTestParametersSubFolders(File testDirectory, String extension) {

    Collection<Object[]> tests = new LinkedList<Object[]>();
    for (File f : testDirectory.listFiles()) {
      if (f.getName().endsWith(extension)) {
        tests.add(new Object[] {f.getPath()});
      }
      if (f.isDirectory()) {
        tests.addAll(
            (Collection<Object[]>)getTestParametersSubFolders(f, ".java"));
      }
    }
    return tests;
  }

  public static String changeExtension(String filename, String newExtension) {
    int index = filename.lastIndexOf('.');
    if (index != -1) {
      return filename.substring(0, index) + newExtension;
    } else {
      return filename + newExtension;
    }
  }

  public static void compareOutput(String actual, File out, File expected) {
    try {
      Files.write(out.toPath(), actual.getBytes());
      assertEquals("Output differs.", readFileToString(expected),
                   normalizeText(actual));
    } catch (IOException e) {
      fail("IOException occurred while comparing output: " + e.getMessage());
    }
  }

  private static String readFileToString(File file)
      throws FileNotFoundException {
    if (!file.isFile()) {
      return "";
    }

    Scanner scanner = new Scanner(file);
    scanner.useDelimiter("\\Z");
    String text = normalizeText(scanner.hasNext() ? scanner.next() : "");
    scanner.close();
    return text;
  }

  private static String normalizeText(String text) {
    return text.replace(SYS_LINE_SEP, "\n").trim();
  }
  private static String SYS_LINE_SEP = System.getProperty("line.separator");
}