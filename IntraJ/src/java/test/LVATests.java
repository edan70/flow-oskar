package test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

import org.extendj.JavaChecker;
import org.extendj.ast.Analysis;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.ConstructorDecl;
import org.extendj.ast.MethodDecl;
import org.extendj.ast.CFGNode;
import org.extendj.ast.Program;
import org.extendj.ast.ReachedLVal;
import org.extendj.ast.WarningMsg;
import org.extendj.flow.utils.IJGraph;
import org.extendj.flow.utils.IJSet;
import org.extendj.flow.utils.Utils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import org.extendj.flow.IntraJ;

@RunWith(Parameterized.class)
public class LVATests {
  // void printLVASets(CFGNode n, PrintStream outStream, IJSet<CFGNode> nodes) {
  //   if (nodes.contains(n))
  //     return;

  //   outStream.println("Node: " + n.CFGName() + "\t\tLVA_in: " +
  //                     n.LVA_in().toStringCFGNode());
  //   outStream.println("Node: " + n.CFGName() + "\t\tLVA_use: " +
  //                     n.LVA_use().toStringCFGNode());
  //   outStream.println("Node: " + n.CFGName() + "\t\tLVA_def: " +
  //                     n.LVA_def().toStringCFGNode());
  //   outStream.println("Node: " + n.CFGName() + "\t\tLVA_out: " +
  //                     n.LVA_out().toStringCFGNode());
  //   nodes.add(n);
  //   for (CFGNode n_child : n.succ()) {
  //     printLVASets(n_child, outStream, nodes);
  //   }
  // }

  public static final File TEST_DIRECTORY = new File("testfiles/DataFlow/LVA");

  private final String filename;

  public LVATests(String testFile) { filename = testFile; }

  @Test
  public void runTest() throws Exception {
    IntraJ.excludeLiteralsAndNull = true;
    PrintStream out = System.out;
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
         PrintStream outStream = new PrintStream(baos)) {
      String[] args = {filename};
      JavaChecker jChecker = new JavaChecker();
      int execCode = jChecker.run(args);
      if (1 == execCode) {
        Assert.fail("Compilation errors found " + execCode);
      }

      IJSet<CFGNode> nodes = new IJSet<>();
      Program program = jChecker.getEntryPoint();
      System.setOut(new PrintStream(baos));
      IJGraph graph = new IJGraph(true, true);
      program.graphLayout(graph);
      program.printGraph(graph);
      for (MethodDecl fun : program.methods()) {
        // printLVASets(fun.Entry(), outStream, nodes);
      }
      for (ConstructorDecl fun : program.constructors()) {
        // printLVASets(fun.Entry(), outStream, nodes);
      }
      Integer nbrWrn = 0;
      for (CompilationUnit cu : program.getCompilationUnits()) {
        for (WarningMsg wm : cu.LVA()) {
          if (wm.getAnalysisType() == Analysis.LVA)
            wm.print(System.out);
          nbrWrn++;
        }
      }

      UtilTest.compareOutput(
          baos.toString(), new File(UtilTest.changeExtension(filename, ".out")),
          new File(UtilTest.changeExtension(filename, ".expected")));

    } finally {
      System.setOut(out);
    }
  }

  @Parameters(name = "{0}")
  public static Iterable<Object[]> getTests() {
    return UtilTest.getTestParametersSubFolders(TEST_DIRECTORY, ".java");
  }
}