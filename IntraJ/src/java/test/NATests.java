package test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;

import org.extendj.JavaChecker;
import org.extendj.ast.Analysis;
import org.extendj.ast.AssignExpr;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.ConstructorDecl;
import org.extendj.ast.MethodDecl;
import org.extendj.ast.CFGNode;
import org.extendj.ast.PostIncExpr;
import org.extendj.ast.Program;

import org.extendj.ast.WarningMsg;
import org.extendj.flow.utils.IJGraph;
import org.extendj.flow.utils.IJSet;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(Parameterized.class)
public class NATests {
  void printNASets(CFGNode n, PrintStream outStream, IJSet<CFGNode> nodes) {
    if (nodes.contains(n))
      return;
    outStream.println("Node: " + n.CFGName() + "\t\tNA_in: " +
                      n.NA_in().toStringCFGNode());
    outStream.println("Node: " + n.CFGName() + "\t\tNA_out: " +
                      n.NA_out().toStringCFGNode());
    outStream.println("Node: " + n.CFGName() + "\t\tNA_gen: " +
                      n.NA_gen().toStringCFGNode());
    outStream.println("Node: " + n.CFGName() + "\t\tNA_kill: " +
                      n.NA_kill().toStringCFGNode());
    nodes.add(n);
    nodes.add(n);
    for (CFGNode n_child : n.succ()) {
      printNASets(n_child, outStream, nodes);
    }
  }

  public static final File TEST_DIRECTORY = new File("testfiles/DataFlow/NA");

  private final String filename;

  public NATests(String testFile) { filename = testFile; }

  @Test
  public void runTest() throws Exception {
    PrintStream out = System.out;
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
         PrintStream outStream = new PrintStream(baos)) {
      String[] args = {filename};
      JavaChecker jChecker = new JavaChecker();
      int execCode = jChecker.run(args);
      if (1 == execCode) {
        Assert.fail("Compilation errors found " + execCode);
      }

      IJSet<CFGNode> nodes = new IJSet<>();
      Program program = jChecker.getEntryPoint();
      System.setOut(new PrintStream(baos));
      IJGraph graph = new IJGraph(true, true);
      program.graphLayout(graph);
      program.printGraph(graph);
      for (MethodDecl fun : program.methods()) {
        // fun.Entry().printNASets(outStream, nodes);
        // printNASets(fun.Entry(), outStream, nodes);
      }
      // for (ConstructorDecl fun : program.constructors()) {
      //   // printNASets(fun.Entry(), outStream, nodes);
      // }
      // outStream.println();
      Integer nbrWrn = 0;
      for (CompilationUnit cu : program.getCompilationUnits()) {
        for (WarningMsg wm : cu.NPA()) {
          if (wm.getAnalysisType() == Analysis.NPA)
            wm.print(System.out);
          nbrWrn++;
        }
      }
      UtilTest.compareOutput(
          baos.toString(), new File(UtilTest.changeExtension(filename, ".out")),
          new File(UtilTest.changeExtension(filename, ".expected")));

    } finally {
      System.setOut(out);
    }
  }

  @Parameters(name = "{0}")
  public static Iterable<Object[]> getTests() {
    return UtilTest.getTestParametersSubFolders(TEST_DIRECTORY, ".java");
  }
}