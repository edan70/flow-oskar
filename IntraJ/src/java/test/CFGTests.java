package test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.ArrayList;

import org.extendj.JavaChecker;
import org.extendj.ast.ConstructorDecl;
import org.extendj.ast.MethodDecl;
import org.extendj.ast.CFGNode;
import org.extendj.ast.Program;
import org.extendj.flow.utils.IJGraph;
import org.extendj.flow.utils.IJSet;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.Assert;

@RunWith(Parameterized.class)
public class CFGTests {

  public static final File TEST_DIRECTORY = new File("testfiles/CFG");

  private final String filename;

  public CFGTests(String testFile) { filename = testFile; }

  @Test
  public void runTest() throws Exception {
    PrintStream out = System.out;
    try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
         PrintStream outStream = new PrintStream(baos)) {
      String[] args = {filename};
      JavaChecker jChecker = new JavaChecker();
      int execCode = jChecker.run(args);
      if (1 == execCode) {
        Assert.fail("Compilation errors found " + execCode);
      }

      IJSet<CFGNode> nodes = new IJSet<>();
      Program program = jChecker.getEntryPoint();
      System.setOut(new PrintStream(baos));
      IJGraph graph = new IJGraph(true, true);
      program.graphLayout(graph);
      program.printGraph(graph);
      for (MethodDecl fun : program.methods()) {
        fun.Entry().printSuccSets(outStream, nodes);
      }
      for (ConstructorDecl fun : program.constructors()) {
        fun.Entry().printSuccSets(outStream, nodes);
      }
      outStream.println();
      nodes.clear();
      for (MethodDecl fun : program.methods()) {
        fun.Exit().printPredSets(outStream, nodes);
      }
      for (ConstructorDecl fun : program.constructors()) {
        fun.Exit().printPredSets(outStream, nodes);
      }
      // graph.generatePDF(filename);
      // {
      //   ArrayList<String> cmdLd = new ArrayList<String>();
      //   cmdLd.add("python3");
      //   cmdLd.add("resources/pdf_merger.py");
      //   cmdLd.add(IJGraph.changeExtension(filename, ""));
      //   ProcessBuilder pb = new ProcessBuilder(cmdLd);
      //   Process process = pb.start();
      //   process.getOutputStream().close();
      //   process.waitFor();
      // }
      // ArrayList<String> cmdLd = new ArrayList<String>();
      // cmdLd.add("python3");
      // cmdLd.add("resources/deleteFiles.py");
      // cmdLd.add(IJGraph.changeExtension(filename, ""));
      // ProcessBuilder pb = new ProcessBuilder(cmdLd);
      // Process process = pb.start();
      // process.getOutputStream().close();
      // process.waitFor();
      UtilTest.compareOutput(
          baos.toString(), new File(UtilTest.changeExtension(filename, ".out")),
          new File(UtilTest.changeExtension(filename, ".expected")));

    } finally {
      System.setOut(out);
    }
  }

  @Parameters(name = "{0}")
  public static Iterable<Object[]> getTests() {
    return UtilTest.getTestParametersSubFolders(TEST_DIRECTORY, ".java");
  }
}