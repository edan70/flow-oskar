package org.extendj.flow;
import java.io.PrintStream;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Arrays;
import org.extendj.flow.utils.IJSet;

import org.extendj.ast.MethodDecl;
import org.extendj.ast.CFGNode;
import org.extendj.JavaChecker;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.Program;
import org.extendj.ast.WarningMsg;
import org.extendj.flow.utils.IJGraph;
import org.extendj.flow.utils.Utils;
import org.extendj.ast.Analysis;

public class IntraJ {
  public enum FlowProfiling { BACKWARD, FORWARD, COLLECTION, NONE, ALL }
  private static Boolean pred = false;
  private static Boolean succ = false;
  private static Boolean debug = false;
  private static Boolean multipleFiles = false;
  public static Boolean excludeLiteralsAndNull = false;
  private static IJGraph graph;
  private static String filename;
  public static Object DrAST_root_node;

  public static ArrayList<Analysis> analysis = new ArrayList<>();

  private static String[] setEnv(String[] args) throws FileNotFoundException {
    if (args.length < 1) {
      System.err.println("You must specify a source file on the command line!");
      printUsage();
      System.exit(1);
    }

    ArrayList<String> FEOptions = new ArrayList<>();

    filename = args[0];
    for (int i = 0; i < args.length; ++i) {
      String opt = args[i];
      if (opt.contains(".java")) {
        FEOptions.add(args[i]);
        continue;
      }
      if (opt.startsWith("-Wall")) {
        analysis.add(Analysis.NPA);
        analysis.add(Analysis.LVA);
        analysis.add(Analysis.ULA);
        analysis.add(Analysis.RAA);
        continue;
      } else if (opt.startsWith("-Wexcept=")) {
        String an = opt.substring(9, opt.length());
        switch (an) {
        case "NPA":
          analysis.remove(Analysis.NPA);
          continue;
        case "LVA":
          analysis.remove(Analysis.LVA);
          continue;
          case "ULA":
          analysis.remove(Analysis.ULA);
          continue;
          case "RAA":
          analysis.remove(Analysis.RAA);
          continue;
        default:
          System.err.println("There is no Analsis with name '" + an + "'");
          printUsage();
        }
      } else if (opt.startsWith("-W")) {
        String an = opt.substring(2, opt.length());
        switch (an) {
        case "NPA":
          analysis.add(Analysis.NPA);
          continue;
        case "LVA":
          analysis.add(Analysis.LVA);
          continue;
          case "ULA":
          analysis.add(Analysis.ULA);
          continue;
          case "RAA":
          analysis.add(Analysis.RAA);
          continue;
        default:
          System.err.println("There is no Analsis with name '" + an + "'");
          printUsage();
        }
      }
      switch (opt) {
      case "-succ":
        succ = true;
        break;
      case "-excludelit":
        excludeLiteralsAndNull = true;
        break;
      case "-pred":
        pred = true;
        break;
      case "-debug":
        pred = true;
        break;
      case "-helpfrontend":
        FEOptions.add("-help");
        break;
      case "-nowarn":
        FEOptions.add("-nowarn");
        break;
      case "-verbose":
        FEOptions.add("-verbose");
        break;

      default:
        System.err.println("Unrecognized option: " + opt);
        printUsage();
      }
    }
    if (FEOptions.size() > 2) {
      multipleFiles = true;
    }
    FEOptions.add("-classpath");
    FEOptions.add(
    "/home/oskar/Desktop/fop/fop-all.jar:/home/oskar/Desktop/fop/aspectjrt.jar:/home/oskar/Desktop/fop-0.95/lib/xmlgraphics-commons-1.3.1.jar:/home/oskar/Desktop/fop-0.95/lib/xml-resolver-1.2.jar:/home/oskar/Desktop/fop-0.95/lib/commons-cli-1.2.jar:/home/oskar/Desktop/fop-0.95/lib/fop-0.20.5.jar:/home/oskar/Desktop/fop-0.95/lib/jai_core-1.1.3.jar:/home/oskar/Desktop/fop-0.95/lib/apache-ant-1.8.2.jar"); 
    return FEOptions.toArray(new String[FEOptions.size()]);
  }

  public static void main(final String[] args)
      throws IOException, InterruptedException {
    int analysisExitValue = 0;
    long startTime = System.nanoTime();
    long parsingTime;
    long analysisTime;
    long totalTime;
    long estimatedTime;

    String[] jCheckerArgs = setEnv(args);
    JavaChecker jChecker = new JavaChecker();
    int exitCode = jChecker.run(jCheckerArgs);
    if (exitCode != 0) {
      System.exit(exitCode);
    }
    Program program = jChecker.getEntryPoint();
    DrAST_root_node = program;
    parsingTime = (System.nanoTime() - startTime);
    estimatedTime = (System.nanoTime() - startTime);
    Utils.printStatistics(
        System.out, "ExtendJ front-end computation complete after(s): " +
                        String.format("%.6f", estimatedTime / 1_000_000000.0));
    Utils.printInfo(System.out, "Generating the CFG");
    analysisExitValue = computeAnalysis(program);
    analysisTime = (System.nanoTime() - startTime);
    estimatedTime = (System.nanoTime() - startTime);
    Utils.printStatistics(
        System.out, "CFG and Dataflow Analysis complete after (s): " +
                        String.format("%.6f", estimatedTime / 1_000_000000.0));
    if (!multipleFiles) {
      DotToPdf(program);
    }
    if (debug) {
      debugSets(program);
    }
    totalTime = (System.nanoTime() - startTime);
    estimatedTime = (System.nanoTime() - startTime);
    Utils.printStatistics(
        System.out, "Total time (s): " +
                        String.format("%.6f", estimatedTime / 1_000_000_000.0));
    Utils.printStatistics(
        System.out, "*---------------------------------------------------*");
    Utils.printStatistics(
        System.out, "ExtendJ front-end time (s): " +
                        String.format("%.6f", parsingTime / 1_000_000_000.0));
    Utils.printStatistics(
        System.out, "CFG and Dataflow Analysis time (s): " +
                        String.format("%.6f", (analysisTime - parsingTime) /
                                                  1_000_000_000.0));
    Utils.printStatistics(System.out,
                          "PDF generation or Fixing time (s): " +
                              String.format("%.6f", (totalTime - analysisTime) /
                                                        1_000_000_000.0));
    ;
  }

  private static void DotToPdf(Program program)
      throws IOException, InterruptedException {
    graph = new IJGraph(pred, succ);
    program.graphLayout(graph);
    program.printGraph(graph);
    Utils.printInfo(System.out, "CFG rendering");
    graph.generatePDF(filename); // TODO: move the process builder inside
                                 // graph.generatePDF(filename)
    Utils.printInfo(System.out, "DOT to PDF");
    ArrayList<String> cmdLd = new ArrayList<String>();
    cmdLd.add("python3");
    cmdLd.add("resources/pdf_merger.py");
    cmdLd.add(IJGraph.changeExtension(filename, ""));
    ProcessBuilder pb = new ProcessBuilder(cmdLd);
    Process process = pb.start();
    process.getOutputStream().close();
    process.waitFor();

    Utils.printInfo(System.out, "PDF file generated correctly");
  }

  private static void printUsage() {
    System.err.println("IntraJ analysis usage:");
    System.err.println("Avaiable analysis:");
    System.err.println("    -NPA: Null pointer analysis");
    System.err.println("    -LVA: Dead assignment analysis");
    System.err.println("Options:");
    System.err.println("    -Wall: all the analysis are computed");
    System.err.println(
        "    -Wexcept=AnalysisID: execute the analysis except 'AnalysisID'");
    System.err.println("    -WAnalysisID: exectute the analysis 'AnalysisID'");
    System.err.println(
        "    -excludelit: common variable initialization are not `Dead assignment`");
    System.err.println("    -helpfrontend: ExtendJ help");
    System.err.println("    -nowarn: suppress ExtendJ warnings");
    System.err.println("    -verbose: ExtendJ runs in verbose mode");
    System.err.println(
        "    -fix (experimental): remove reported `Dead assignments` ");
    System.err.println(
        "    -fixall (experimental): executes the analysis several times with `-fix` option. Ends when no more warnings are detected.");
    System.err.println("    -succ: the .pdf file includes successors");
    System.err.println("    -pred: the .pdf file includes predecessors");
    System.err.println("    -debug: prints the analysis set.");
    System.exit(1);
  }

  private static int computeAnalysis(Program program) {
    Integer nbrWrn = 0;
    for (CompilationUnit cu : program.getCompilationUnits()) {
      for (Analysis a : analysis) {
        try {
          TreeSet<WarningMsg> wmgs = (TreeSet<WarningMsg>)cu.getClass()
                                         .getDeclaredMethod(a.toString())
                                         .invoke(cu);
          for (WarningMsg wm : wmgs) {
            if (analysis.contains(wm.getAnalysisType())) {
              wm.print(System.out);
              nbrWrn++;
            }
          }
        } catch (Throwable t) {
          t.printStackTrace();
          System.exit(1);
        }
      }
    }
    Utils.printStatistics(System.out, "Total number of warnings: " + nbrWrn);
    return nbrWrn;
  }

  // TODO(idrissrio): use reflection to improve this function.
  static void debugSets(Program program) {
    for (MethodDecl fun : program.methods()) {
      fun.Entry().printNASets(System.out, new IJSet<CFGNode>());
    }
  }
}