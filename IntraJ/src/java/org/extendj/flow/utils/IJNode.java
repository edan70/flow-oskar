package org.extendj.flow.utils;

import org.extendj.ast.ASTNode;

public class IJNode {
  Integer AEA_in = 0;
  Integer AEA_out = 0;
  Integer LVA_in = 0;
  Integer LVA_out = 0;
  Integer VBE_in = 0;
  Integer VBE_out = 0;
  Integer NA_in = 0;
  Boolean isDeadAssign = false;
  String prettyPrint;
  Integer NA_out = 0;
  String color = "";
  String complete_ID = "";
  String simple_ID = "";
  String dotDescription = "";
  String dotDependencies = "";
  Boolean isCFGNode = false;
  Integer rank = 0;

  /**
   * The complete_ID is composed by ID[x,y][z,w].
   * simple_ID will be just ID
   */
  public IJNode(ASTNode astnode) {
    String complete_ID = astnode.CFGName();
    this.setDependency(astnode.nodeOrder());
    this.setPrettyPrint(astnode.CFGDump());
    this.setRank(astnode.getRank());
    this.complete_ID = complete_ID;
    this.simple_ID = complete_ID.indexOf("[") == -1
                         ? complete_ID
                         : complete_ID.substring(0, complete_ID.indexOf("["));
    setIsCFGNode(false);
  }

  public void setIsCFGNode(Boolean isCFGNode) {
    this.isCFGNode = isCFGNode;
    if (!isCFGNode)
      color = "style= dotted   fillcolor=\"#eeeeee\" fontcolor=\"#aaaaaa\"";
    else
      color = "fillcolor=white   style=filled";
  }

  public Boolean isCFGNode() { return this.isCFGNode; }

  public void setDependency(String dep) { dotDependencies = dep; }
  public String getDependency() { return dotDependencies; }

  public void setIsDeadAssign(Boolean isDeadAssign) {
    this.isDeadAssign = isDeadAssign;
    if (isDeadAssign)
      color = "fillcolor=\"1 0.2 1\"   style=filled";
    else
      color = "fillcolor=white   style=filled";
  }

  public Boolean getIsDeadAssign() { return isDeadAssign; }

  public String getSimpleName() { return simple_ID; }

  public String getID() { return complete_ID; }

  public Integer getRank() { return rank; }

  public void setRank(Integer i) { this.rank = i; }

  public void setColor(String color) { this.color = color; }

  public String getColor() { return color; }

  public Integer getAEA_in() { return AEA_in; }

  public Integer getVBE_in() { return VBE_in; }

  public Integer getNA_in() { return NA_in; }

  public Integer getLVA_in() { return LVA_in; }

  public Integer getAEA_out() { return AEA_out; }

  public Integer getVBE_out() { return VBE_out; }

  public Integer getNA_out() { return NA_out; }

  public Integer getLVA_out() { return LVA_out; }

  public String getPrettyPrint() { return prettyPrint; }

  public void setPrettyPrint(String pp) { prettyPrint = pp; }

  public void incrementIndex(String method) {
    switch (method) {
    case "LVA_in()":
      ++LVA_in;
      break;
    case "LVA_out()":
      ++LVA_out;
      break;
    case "VBE_in()":
      ++VBE_in;
      break;
    case "VBE_out()":
      ++VBE_out;
      break;
    case "NA_in()":
      ++NA_in;
      break;
    case "NA_out()":
      ++NA_out;
      break;
    case "AEA_in()":
      ++AEA_in;
      break;
    case "AEA_out()":
      ++AEA_out;
      break;
    }
  }

  public String getDotDescription() {
    dotDescription = "\"" + getID() + "\" [label=\"" + getSimpleName() +
                     "\\n " + getPrettyPrint() + "\", " + getColor() + "  ]\n";

    return dotDescription;
  }
}