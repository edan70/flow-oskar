package org.extendj.flow.utils;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;
import org.extendj.ast.CFGNode;
import org.extendj.ast.SimpleSet;

public class IJSet<E> extends LinkedHashSet<E> {
  public IJSet() { super(); }

  public IJSet(LinkedHashSet<E> sc) { super(sc); }

  public IJSet(IJSet<E> sc) { super(sc); }

  public IJSet(Set<E> sc) { super(sc); }

  public IJSet(E e) { super(Collections.singleton(e)); }

  public static IJSet emptySet() { return new IJSet<>(); }

  public static <E> IJSet<E> singleton(E e) { return new IJSet<E>(e); }

  /**
   * Remove all elements in @param this and @return @param this with side
   * effects.
   */
  public IJSet<E> _removeAll(IJSet<E> set) {
    removeAll(set);
    return this;
  }

  public IJSet<E> _addAll(Set<E> set) {
    addAll(set);
    return this;
  }

  public IJSet<E> _remove(E e) {
    remove(e);
    return this;
  }

  public E get(int i) {
    if (i < 0 || i > size())
      throw new RuntimeException();
    int j = 0;
    for (E e : this) {
      if (j == i)
        return e;
      j++;
    }
    return null;
  }

  public IJSet<E> intersection(IJSet<E> set) {
    IJSet<E> res = new IJSet<E>();
    for (E obj : this) {
      for (E expr : set) {
        if (obj.equals(expr)) {
          res.add(obj);
          continue;
        }
      }
    }
    return res;
  }

  public void _intersection(IJSet<E> set) {
    IJSet<E> res = new IJSet<E>();
    for (E obj : this) {
      for (E expr : set) {
        if (obj == expr) {
          res.add(obj);
          continue;
        }
      }
    }
    clear();
    addAll(res);
  }

  public void _intersectionStr(IJSet<E> set) {
    IJSet<E> res = new IJSet<E>();
    for (E obj : this) {
      for (E expr : set) {
        if (obj.equals(expr)) {
          res.add(obj);
          continue;
        }
      }
    }
    clear();
    addAll(res);
  }

  public String toStringCFGNode() {
    String str = "[";
    for (E obj : this) {
      try {
        CFGNode node = (CFGNode)obj;
        str += node.CFGName() + ", ";
      } catch (ClassCastException t) {
        str += obj.toString() + ",";
      }
    }
    if (str.indexOf(", ") == -1)
      return str + "]";
    else
      return str = str.substring(0, str.length() - 2) + "]";
  }
}