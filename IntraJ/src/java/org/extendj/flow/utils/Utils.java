package org.extendj.flow.utils;

import java.io.PrintStream;

public class Utils {
  private static final String RESET = "\033[0m";          // Text Reset
  private static final String YELLOW = "\033[0;33m";      // YELLOW
  private static final String YELLOW_BOLD = "\033[1;33m"; // YELLOW_BOLD
  private static final String GREEN_BOLD = "\033[1;32m";  // GREEN
  private static final String RED_BOLD = "\033[1;31m";    // RED
  private static final String WHITE_BOLD = "\033[1;37m";  // WHITE
  public static void printWarning(PrintStream out, String location,
                                  String msg) {
    out.print(RED_BOLD);
    out.print("[" + location + "]: ");
    out.print(WHITE_BOLD);
    out.println(msg);
    out.print(RESET);
  }
  public static void printInfo(PrintStream out, String msg) {
    out.print(GREEN_BOLD);
    out.print("[INFO]: ");
    out.print(WHITE_BOLD);
    out.println(msg);
    out.print(RESET);
  }

  public static void printStatistics(PrintStream out, String msg) {
    out.print(YELLOW_BOLD);
    out.print("[STATISTIC]: ");
    out.print(WHITE_BOLD);
    out.println(msg);
    out.print(RESET);
  }

  public static String[] removeElement(String[] arr, int index) {
    if (arr == null || index < 0 || index >= arr.length) {
      return arr;
    }
    String[] anotherArray = new String[arr.length - 1];
    for (int i = 0, k = 0; i < arr.length; i++) {
      if (i == index) {
        continue;
      }
      anotherArray[k++] = arr[i];
    }
    return anotherArray;
  }
}