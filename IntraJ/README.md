
# Oskar_program_analysis

**Oskar_program_analysis** is an extenstion of **IntraJ** which in turn is an extension of **[ExtendJ](https://extendj.org)** Java Compiler. The ExtendJ code is released under 2-clause BSD license. This extension includes:

- Reduntant Assignment analysis.

## Prerequisite

To run the analysis you need

1) **Java SDK version 8**


To install the Python dependencies run the instruction described in the next section.
## Build and Usage


To generate the Jar file, execute

```
./gradlew jar
```

To run the analysis use

```
java -jar "path_to_IntraJ.jar" "Path_to_file" -WRAA
```

In above path_to_IntraJ.jar is the path to the jar file generated with the previous command. By default this file is in the IntraJ folder. The Path_to_file is the path to the file to run the analysis on.

In the folder oskar_testfiles there are example java programs that can be used to run the tests on.

## Python Dependencies

To install Python dependencies, you can execute the following instruction:

```
cd resources
pip3 install - requirements.txt
```
